﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Common;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Serialize Client";
            Console.OutputEncoding = Encoding.UTF8;
            while (true)
            {
                Console.WriteLine("Press enter to send ...");
                Console.ReadLine();
                Student student = new Student
                {
                    Id = 1,
                    FirstName = "Đào Văn",
                    LastName = "Thương",
                    DateOfBirth = new DateTime(2001, 04, 20)
                };
                TcpClient client = new TcpClient();
                client.Connect(IPAddress.Loopback, 1308);
                NetworkStream stream = client.GetStream();
                StreamWriter sw = new StreamWriter(stream) { AutoFlush = true };
                sw.WriteLine(TextSerializer.Serialize(student));

            }
        }
    }
}
