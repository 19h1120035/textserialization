﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class TextSerializer
    {
        // Chuyển đổi 1 object của student sang chuỗi kí tự
        // Chuyển kết quả có hình thức tương tự chuỗi tham số Http get
        public static string Serialize(Student student)
        {
            return $"ID = {student.Id} " +
                $"& FirstName = {student.FirstName} " +
                $"& LastName = {student.LastName} " +
                $"& DateOfBirth = {student.DateOfBirth.Ticks} ";
        }
        // Chuyển đổi 1 chuỗi trở lại thành object kiểu student
        public static Student Deserialize(string data)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            string[] pairs = data.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var pair in pairs)
            {
                var p = pair.Split('='); // cắt mỗi phần tử lấy mốc là kí tự =
                if (p.Length == 2) // 1 cặp khóa = gia_tri đứng sau khi cắt sẽ phải có 2 phần
                {
                    string key = p[0].Trim(); // phần tử thứ nhất là khóa
                    string value = p[1].Trim(); // phần tử thứ hai là giá trị
                    dict[key] = value; // lưu cặp khóa gia_tri này lại sử dụng phép toán indexing
                }
            }
            Student student = new Student();
            if (dict.ContainsKey("Id"))
            {
                student.Id = int.Parse(dict["Id"]);
            }
            if (dict.ContainsKey("FirstName"))
            {
                student.FirstName = dict["FirstName"];
            }
            if (dict.ContainsKey("LastName"))
            {
                student.LastName = dict["LastName"];
            }
            if (dict.ContainsKey("DateOfBirth"))
            {
                student.DateOfBirth = new DateTime(long.Parse(dict["DateOfBirth"]));
            }
            return student;
        }
    }
}
