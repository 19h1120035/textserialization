﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Console.Title = "Serialize Server";
            TcpListener listener = new TcpListener(IPAddress.Any, 1308);
            listener.Start(10);
            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();
                NetworkStream stream = client.GetStream();
                StreamReader sr = new StreamReader(stream);

                string data = sr.ReadLine();
                Student student = TextSerializer.Deserialize(data);
                Console.WriteLine($"Raw data:\r\n {data}\r\n");
                Console.WriteLine("Deserialized object: ");
                Console.WriteLine($"ID: {student.Id}\r\nFirstName: {student.FirstName}\r\nLastName: {student.LastName}\r\n" +
                    $"DateOfBirth: {student.DateOfBirth.ToShortDateString()}");
                client.Close();
            }
        }
    }
}
